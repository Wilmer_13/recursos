class Actividad:
    def __init__ (selft, ident, nombre):
            selft.identificador = ident
            selft.nombre = nombre
actividad = Actividad("Preguntas", "- el sistema esquelético")
print(actividad.identificador, actividad.nombre)
print()

class Pregunta:
    def __init__ (self, num, texto, opc1, opc2, opc3, respuesta):
            self.numero = num
            self.texto = texto
            self.opcion1 = opc1
            self.opcion2 = opc2
            self.opcion3 = opc3
            self.respuesta = respuesta

pregunta = Pregunta(1, "¿Cuáles son los tipos de huesos del cuerpo humano?", "planos,largos, cortos", "angostos,altos, pequeños", "grandes, fusiformes, delgados", "planos, largos, cortos")
print("pregunta Número: ", pregunta.numero)
print("pregunta: ", pregunta.texto)
print("opción 1: ", pregunta.opcion1)
print("opción 2: ", pregunta.opcion2)
print("opción 3: ", pregunta.opcion3)
print("respuesta: ", pregunta.respuesta)

class Pregunta2:
        def __init__ (self, num, texto, opc1, opc2, opc3, respuesta):
             self.numero = num
             self.texto = texto
             self.opcion1 = opc1
             self.opcion2 = opc2
             self.opcion3 = opc3
             self.respuesta = respuesta

pregunta2 = Pregunta2(2, "¿Cómo se divide el cuerpo humano?", "piernas, tórax, cabeza", "cráneo, tórax, extremidades", "cráneo, brasos, extremidades", "cráneo, tórax, extremidades")
print ()
print("pregunta Número: ", pregunta2.numero)
print("pregunta: ", pregunta2.texto)
print("opción 1: ", pregunta2.opcion1)
print("opción 2: ", pregunta2.opcion2)
print("opción 3: ", pregunta2.opcion3)
print("respuesta: ", pregunta2.respuesta)


class Pregunta3:
    def __init__(self, num, texto, opc1, opc2, opc3, respuesta):
        self.numero = num
        self.texto = texto
        self.opcion1 = opc1
        self.opcion2 = opc2
        self.opcion3 = opc3
        self.respuesta = respuesta


pregunta3 = Pregunta3(3, "¿Cómo se clacifican las articulaciones?", "fijas, semiestáticas, moviles", "fijas, semimóviles, móviles","estáticas, semimóviles, móviles", "fijas, semimóviles, moviles")
print()
print("pregunta Número: ", pregunta3.numero)
print("pregunta: ", pregunta3.texto)
print("opción 1: ", pregunta3.opcion1)
print("opción 2: ", pregunta3.opcion2)
print("opción 3: ", pregunta3.opcion3)
print("respuesta: ", pregunta3.respuesta)


